export default Object.freeze({
  LOGIN: 'Login',
  REGISTER: 'Register',
  CHANGE_PASSWORD: 'Password',
  CHOOSE_WAREHOUSE: 'ChooseWareHouse',
  HOME: 'Home',
  AUTH: 'Auth',
  APP: 'App',
});
