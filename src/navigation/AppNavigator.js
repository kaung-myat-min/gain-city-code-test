import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import routes from './routes';
import Home from '../screens/home/HomeScreen';
import ChooseWareHouseScreen from '../screens/ChooseWareHouseScreen';

const Stack = createStackNavigator();

const AppNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name={routes.CHOOSE_WAREHOUSE}
      options={{headerShown: false}}
      component={ChooseWareHouseScreen}
    />
    <Stack.Screen
      name={routes.HOME}
      component={Home}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

export default AppNavigator;
