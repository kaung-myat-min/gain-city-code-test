export default {
  primary: '#F5A800',
  secondary: '#403F40',
  black: '#000',
  white: '#fff',
  medium: '#6e6969',
  light: '#f8f4f4',
  dark: '#0c0c0c',
  danger: '#ff5252',
  gradientStart: '#F4A913',
  gridientMiddle: '#F5B224',
  gradientEnd: '#FCFBF9',
  transparent: '#ffffff00',
};
