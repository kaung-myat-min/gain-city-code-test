import React from 'react';
import {useFormikContext} from 'formik';

import AppButton from '../common/AppButton';
import defaultStyle from '../../config/styles';

function AppSubmitButton({title}) {
  const {handleSubmit} = useFormikContext();

  return (
    <AppButton
      title={title}
      onPress={handleSubmit}
      color={defaultStyle.colors.white}
    />
  );
}

export default AppSubmitButton;
