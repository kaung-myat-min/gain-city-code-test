import React from 'react';
import {TouchableWithoutFeedback, StyleSheet, Image, View} from 'react-native';

import AppText from '../common/AppText';
import defaultStyle from '../../config/styles';

function HomeTabButton({icon, title, style, isSelected, onPress}) {
  return (
    <TouchableWithoutFeedback onPress={() => onPress()}>
      <View
        style={[
          styles.container,
          {
            backgroundColor: isSelected
              ? defaultStyle.colors.white
              : defaultStyle.colors.transparent,
          },
          style,
        ]}>
        <Image style={styles.icon} source={icon} width={40} height={40} />
        <AppText style={styles.title}>{title}</AppText>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: defaultStyle.colors.white,
    borderWidth: 1,
    borderRadius: 10,
    padding: 8,
  },
  icon: {},
  title: {
    fontWeight: '700',
    marginStart: 4,
    flex: 1,
    textTransform: 'uppercase',
  },
});

export default HomeTabButton;
