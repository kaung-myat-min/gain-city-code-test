import React from 'react';
import {View, StyleSheet, FlatList, ScrollView} from 'react-native';

import AppText from '../common/AppText';
import defaultStyles from '../../config/styles';
import HomeFilterButton from './HomeFilterButton';
import CategoryItem from './CategoryItem';

const categories = [
  {
    id: '1',
    image: require('../../assets/ic_accessories.png'),
    title: 'Accessories',
  },
  {
    id: '2',
    image: require('../../assets/ic_bolt_nut.png'),
    title: 'Bolt Nut',
  },
  {
    id: '3',
    image: require('../../assets/ic_cable_tray.png'),
    title: 'Cable Tray',
  },
  {
    id: '4',
    image: require('../../assets/ic_accessories.png'),
    title: 'Accessories2',
  },
  {
    id: '5',
    image: require('../../assets/ic_bolt_nut.png'),
    title: 'Bolt Nut',
  },
  {
    id: '6',
    image: require('../../assets/ic_cable_tray.png'),
    title: 'Cable Tray',
  },
  {
    id: '7',
    image: require('../../assets/ic_accessories.png'),
    title: 'Accessories',
  },
  {
    id: '8',
    image: require('../../assets/ic_bolt_nut.png'),
    title: 'Bolt Nut',
  },
  {
    id: '9',
    image: require('../../assets/ic_cable_tray.png'),
    title: 'Cable Tray',
  },
];
const filters = [
  {id: '1', title: 'system', backgroundColor: '#122E4D'},
  {id: '2', title: 'commerical', backgroundColor: '#E83D27'},
  {id: '3', title: 'air curtain', backgroundColor: '#2DA091'},
  {id: '4', title: 'phone', backgroundColor: '#122E4D'},
];
function CategoryListFragment(props) {
  return (
    <>
      <View style={styles.filterContainer}>
        <AppText style={styles.label}>Recently</AppText>
        <ScrollView horizontal contentContainerStyle={styles.filterButtons}>
          {filters.map(filter => (
            <HomeFilterButton
              key={filter.id}
              title={filter.title}
              backgroundColor={filter.backgroundColor}
            />
          ))}
        </ScrollView>
      </View>

      <AppText style={styles.label}>Category</AppText>
      <FlatList
        style={styles.categoryList}
        data={categories}
        keyExtractor={item => item.id}
        numColumns={3}
        renderItem={({item}) => <CategoryItem image={item.image} />}
      />
    </>
  );
}

const styles = StyleSheet.create({
  filterContainer: {
    backgroundColor: defaultStyles.colors.primary,
    paddingVertical: 10,
  },
  label: {
    textTransform: 'uppercase',
    fontWeight: '200',
    marginStart: 10,
    marginVertical: 4,
  },
  filterButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  categoryList: {
    marginHorizontal: 8,
  },
});

export default CategoryListFragment;
