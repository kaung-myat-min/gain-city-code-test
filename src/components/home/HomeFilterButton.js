import React from 'react';
import {StyleSheet, TouchableOpacity, View, Dimensions} from 'react-native';

import defaultStyles from '../../config/styles';
import AppText from '../common/AppText';
function HomeFilterButton({backgroundColor, title}) {
  return (
    <TouchableOpacity>
      <View
        style={[
          styles.container,
          {
            backgroundColor: backgroundColor,
            width: Dimensions.get('screen').width * 0.31,
          },
        ]}>
        <AppText style={styles.text}>{title}</AppText>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 15,
    paddingHorizontal: 8,
    borderRadius: 8,
    marginHorizontal: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: defaultStyles.colors.light,
    textTransform: 'uppercase',
    fontSize: 14,
  },
});

export default HomeFilterButton;
