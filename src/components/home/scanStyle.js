import {Dimensions} from 'react-native';
import defaultStyles from '../../config/styles';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const styles = {
  bgQr: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sampleQr: {
    width: 100,
    height: 100,
  },
  sampleQrBackground: {
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    padding: 20,
  },
  scanBack: {
    width: 64,
    height: 64,
    marginBottom: 25,
  },
  scrollViewStyle: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: defaultStyles.colors.secondary,
  },
  cardView: {
    width: deviceWidth - 32,
    height: deviceHeight - 350,
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRadius: 10,
    padding: 25,
    marginLeft: 5,
    marginRight: 5,
    marginTop: '10%',
    backgroundColor: 'white',
  },
  scanCardView: {
    width: deviceWidth - 32,
    height: deviceHeight / 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: 25,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'white',
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonScan: {
    borderWidth: 2,
    borderRadius: 10,
    borderColor: defaultStyles.colors.primary,
    paddingTop: 5,
    paddingRight: 25,
    paddingBottom: 5,
    paddingLeft: 25,
    marginTop: 20,
  },
  buttonScan2: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
  },
  centerText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    padding: 32,
    color: 'white',
  },
  bottomContent: {
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
    height: 120,
  },
  buttonTouchable: {
    fontSize: 21,
    backgroundColor: 'white',
    marginTop: 32,
    width: deviceWidth - 62,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
  },
  buttonTextStyle: {
    color: defaultStyles.colors.primary,
    fontWeight: 'bold',
  },
};
export default styles;
