import React from 'react';
import {TouchableOpacity, StyleSheet, Image} from 'react-native';

function CategoryItem({image}) {
  return (
    <TouchableOpacity style={styles.container}>
      <Image style={styles.image} source={image} resizeMode="stretch" />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    flex: 1,
    width: 100,
    height: 120,
    margin: 2,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
  },
});

export default CategoryItem;
