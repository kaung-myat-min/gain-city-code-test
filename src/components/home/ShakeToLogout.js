import React, {useEffect} from 'react';
import {Alert, StyleSheet} from 'react-native';
import RNShake from 'react-native-shake';

import useAuth from '../../auth/useAuth';
let alertShow = false;
function ShakeToLogout(props) {
  const {logOut} = useAuth();
  const logoutConfirmation = () => {
    if (alertShow) return;

    alertShow = true;

    Alert.alert('Logout', 'Are you sure to logout?', [
      {
        text: 'Cancel',
        onPress: () => (alertShow = false),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => logOut()},
    ]);
  };

  useEffect(() => {
    const subscription = RNShake.addListener(() => {
      logoutConfirmation();
    });
    return () => {
      RNShake.remove(subscription);
    };
  }, []);

  return null;
}

const styles = StyleSheet.create({
  container: {},
});

export default ShakeToLogout;
