import React, {Component, Fragment} from 'react';
import {
  TouchableOpacity,
  Text,
  Linking,
  View,
  Image,
  ImageBackground,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './scanStyle';
class ScanFragment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scan: false,
      ScanResult: false,
      result: null,
    };
  }
  onSuccess = e => {
    const check = e.data.substring(0, 4);
    console.log('scanned data' + check);
    this.setState({
      result: e,
      scan: false,
      ScanResult: true,
    });
    if (check === 'http') {
      Linking.openURL(e.data).catch(err =>
        console.error('An error occured', err),
      );
    } else {
      this.setState({
        result: e,
        scan: false,
        ScanResult: true,
      });
    }
  };
  activeQR = () => {
    this.setState({scan: true});
  };
  scanAgain = () => {
    this.setState({scan: true, ScanResult: false});
  };
  render() {
    const {scan, ScanResult, result} = this.state;
    return (
      <View style={styles.scrollViewStyle}>
        <>
          {!scan && !ScanResult && (
            <ImageBackground
              source={require('../../assets/bg_qr.jpeg')}
              style={styles.bgQr}
              resizeMode="cover"
              blurRadius={10}>
              <TouchableOpacity onPress={this.activeQR}>
                <View style={styles.sampleQrBackground}>
                  <Image
                    source={require('../../assets/sample_qr.png')}
                    style={styles.sampleQr}
                  />
                </View>
              </TouchableOpacity>
            </ImageBackground>
          )}
          {ScanResult && (
            <>
              <View style={ScanResult ? styles.scanCardView : styles.cardView}>
                <Text>Type : {result.type}</Text>
                <Text>Result : {result.data}</Text>
                <Text numberOfLines={1}>RawData: {result.rawData}</Text>
                <TouchableOpacity
                  onPress={this.scanAgain}
                  style={styles.buttonScan}>
                  <View style={styles.buttonWrapper}>
                    <Image
                      source={require('../../assets/camera.png')}
                      style={{height: 36, width: 36}}
                    />
                    <Text style={{...styles.buttonTextStyle}}>
                      Click to scan again
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </>
          )}
          {scan && (
            <QRCodeScanner
              reactivate={true}
              showMarker={true}
              ref={node => {
                this.scanner = node;
              }}
              onRead={this.onSuccess}
              topContent={
                <Text style={styles.centerText}>
                  Please move your camera {'\n'} over the QR Code
                </Text>
              }
              bottomContent={
                <View>
                  <ImageBackground
                    source={require('../../assets/bottom-panel.png')}
                    style={styles.bottomContent}>
                    <TouchableOpacity
                      style={styles.buttonScan2}
                      onPress={() => this.setState({scan: false})}>
                      <Image
                        source={require('../../assets/ic_back.png')}
                        style={styles.scanBack}
                      />
                    </TouchableOpacity>
                  </ImageBackground>
                </View>
              }
            />
          )}
        </>
      </View>
    );
  }
}
export default ScanFragment;
