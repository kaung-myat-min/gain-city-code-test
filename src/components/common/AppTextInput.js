/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import defaultStyles from '../../config/styles';
import AppText from './AppText';

const renderPlaceholder = placeholder => {
  return <AppText style={{paddingTop: 6}}>{placeholder + ':'}</AppText>;
};

function AppTextInput({icon, placeholder, ...otherProps}) {
  const canShowPlaceHolderView = otherProps.value !== '';
  return (
    <View
      style={[
        styles.container,
        {
          justifyContent: canShowPlaceHolderView ? 'flex-start' : 'center',
        },
      ]}>
      {icon && (
        <Icon
          name={icon}
          size={20}
          color={defaultStyles.colors.black}
          style={styles.icon}
        />
      )}

      {canShowPlaceHolderView && renderPlaceholder(placeholder)}

      <TextInput
        placeholderTextColor={defaultStyles.colors.black}
        placeholder={canShowPlaceHolderView ? '' : placeholder}
        width="100%"
        style={[
          styles.textInput,
          defaultStyles.text,
          {
            paddingEnd: icon ? 40 : 0,
            textAlign: canShowPlaceHolderView ? 'left' : 'center',
          },
        ]}
        {...otherProps}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: defaultStyles.colors.white,
    borderBottomWidth: 2,
  },
  icon: {
    marginRight: 10,
    marginLeft: 10,
  },
  textInput: {
    textAlign: 'center',
    paddingBottom: 3,
    width: '100%',
  },
});

export default AppTextInput;
