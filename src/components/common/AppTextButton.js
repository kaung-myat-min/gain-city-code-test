import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

import colors from '../../config/colors';

function AppTextButton({title, onPress, color}) {
  return (
    <TouchableOpacity
      style={[styles.button, {backgroundColor: color}]}
      onPress={onPress}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    width: '100%',
  },
  text: {
    color: colors.black,
    fontSize: 16,
    fontWeight: '600',
  },
});

export default AppTextButton;
