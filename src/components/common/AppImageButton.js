import React from 'react';
import {TouchableOpacity, StyleSheet, Image} from 'react-native';
import defaultStyles from '../../config/styles';

function AppImageButton({image, onPress, style}) {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.container, style]}>
      <Image source={image} style={styles.image} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: defaultStyles.colors.white,
    padding: 1,
    borderRadius: 8,
    margin: 2,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: 100,
  },
});

export default AppImageButton;
