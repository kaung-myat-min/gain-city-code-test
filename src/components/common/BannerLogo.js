import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import AppText from './AppText';

function BannerLogo({style = {}}) {
  return (
    <View style={[styles.logoContainer, style]}>
      <Image style={styles.logo} source={require('../../assets/ic_logo.png')} />
      <AppText style={styles.logoTitle}>Gain City</AppText>
    </View>
  );
}

const styles = StyleSheet.create({
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 20,
  },
  logo: {
    width: 80,
    height: 80,
    alignSelf: 'center',
  },
  logoTitle: {
    textTransform: 'uppercase',
    fontSize: 30,
    fontWeight: '700',
  },
});

export default BannerLogo;
