import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

import colors from '../../config/colors';

function AppButton({title, onPress, color}) {
  return (
    <TouchableOpacity
      style={[styles.button, {backgroundColor: color}]}
      onPress={onPress}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    width: '100%',
    marginVertical: 10,
  },
  text: {
    color: colors.black,
    fontSize: 16,
    textTransform: 'uppercase',
    fontWeight: '600',
  },
});

export default AppButton;
