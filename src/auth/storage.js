import AsyncStorage from '@react-native-async-storage/async-storage';

const fetchUser = async () => {
  try {
    const user = await AsyncStorage.getItem('user');
    if (!user) return null;

    return JSON.parse(user);
  } catch (error) {
    console.log(error);
  }
};

const storeUser = async user => {
  try {
    await AsyncStorage.setItem('user', JSON.stringify(user));
  } catch (error) {
    console.log(console.error());
  }
};

export default {
  fetchUser,
  storeUser,
};
