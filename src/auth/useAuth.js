import {useContext} from 'react';

import AuthContext from './context';
import authStorage from './storage';

const useAuth = () => {
  const {user, setUser} = useContext(AuthContext);

  const logIn = data => {
    setUser(data);
    authStorage.storeUser(data);
  };

  const logOut = () => {
    setUser(null);
    authStorage.storeUser(null);
  };

  return {user, logIn, logOut};
};

export default useAuth;
