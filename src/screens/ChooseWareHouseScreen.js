import React from 'react';
import {StyleSheet, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Yup from 'yup';

import Screen from '../components/common/Screen';
import defaultStyle from '../config/styles';
import routes from '../navigation/routes';
import BannerLogo from '../components/common/BannerLogo';
import AppImageButton from '../components/common/AppImageButton';
import {AppForm, AppFormField} from '../components/form';
import AppText from '../components/common/AppText';
import useAuth from '../auth/useAuth';

const validationSchema = Yup.object().shape({
  staffId: Yup.string().required().label('StaffId'),
  password: Yup.string().required().min(4).label('Password'),
});

const wareHouses = [
  {
    id: 1,
    name: 'Sk warehouse',
    image: require('../assets/sk-wh.png'),
  },
  {
    id: 2,
    name: 'Amk warehouse',
    image: require('../assets/amk-wh.png'),
  },
];
const handleWareHouseSelect = (wareHouse, navigation) => {
  navigation.navigate(routes.HOME, wareHouse);
};

const getTodayDate = () => {
  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = today.getFullYear();

  return dd + '/' + mm + '/' + yyyy;
};

function ChooseWareHouseScreen({navigation}) {
  const {user} = useAuth();

  const handleSubmit = ({staffId, name}) => {
    console.log(staffId, name);
  };

  return (
    <Screen style={{backgroundColor: defaultStyle.colors.primary}}>
      <LinearGradient
        colors={[
          defaultStyle.colors.gradientStart,
          defaultStyle.colors.gridientMiddle,
          defaultStyle.colors.gradientEnd,
        ]}
        style={styles.linearGradient}>
        <BannerLogo style={styles.logo} />

        <View style={styles.dateContainer}>
          <AppText style={styles.dateLable}>Date:</AppText>
          <AppText style={styles.date}>{getTodayDate()}</AppText>
        </View>

        <AppForm
          initialValues={{staffId: user.id, name: user.name}}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}>
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            name="staffId"
            placeholder="Staff Id"
            editable={false}
          />
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            name="name"
            placeholder="Name"
            editable={false}
          />
          <AppText style={styles.label}>Select Warehouse:</AppText>
          <View style={styles.wareHouseContainer}>
            {wareHouses.map(wareHouse => (
              <AppImageButton
                key={wareHouse.id.toString()}
                image={wareHouse.image}
                onPress={() => handleWareHouseSelect(wareHouse, navigation)}
                style={styles.warehouse}
              />
            ))}
          </View>
        </AppForm>
      </LinearGradient>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {},
  logo: {
    marginTop: 40,
    marginBottom: 10,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 5,
  },
  wareHouseContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  warehouse: {
    flex: 1,
  },
  label: {
    marginTop: 10,
  },
  dateLable: {},
  date: {
    color: defaultStyle.colors.white,
    marginStart: 4,
  },
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 70,
  },
});

export default ChooseWareHouseScreen;
