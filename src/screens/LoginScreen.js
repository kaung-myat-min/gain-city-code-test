import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Yup from 'yup';

import AppTextButton from '../components/common/AppTextButton';
import Screen from '../components/common/Screen';
import defaultStyle from '../config/styles';
import routes from '../navigation/routes';
import BannerLogo from '../components/common/BannerLogo';
import {
  ErrorMessage,
  AppForm,
  AppFormField,
  AppSubmitButton,
} from '../components/form';

import useAuth from '../auth/useAuth';

const validationSchema = Yup.object().shape({
  staffId: Yup.string().required().label('StaffId'),
  password: Yup.string().required().min(4).label('Password'),
});

function LoginScreen({navigation}) {
  const {logIn} = useAuth();

  const handleSubmit = ({staffId, password}) => {
    //todo: login with real data
    logIn({id: staffId, name: 'Jackie Chan'});
  };

  return (
    <Screen style={{backgroundColor: defaultStyle.colors.primary}}>
      <LinearGradient
        colors={[
          defaultStyle.colors.gradientStart,
          defaultStyle.colors.gridientMiddle,
          defaultStyle.colors.gradientEnd,
        ]}
        style={styles.linearGradient}>
        <BannerLogo style={styles.logo} />
        <AppForm
          initialValues={{staffId: '', password: ''}}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}>
          {/* <ErrorMessage
            error="Invalid email and/or password."
            visible={loginFailed}
          /> */}
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            name="staffId"
            placeholder="Staff Id"
          />
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            name="password"
            placeholder="Password"
            secureTextEntry
            textContentType="password"
          />
          <AppSubmitButton title="Login" />
          <AppTextButton
            title="Change Password?"
            color={defaultStyle.colors.transparent}
            onPress={() => navigation.navigate(routes.CHANGE_PASSWORD)}
          />
        </AppForm>
      </LinearGradient>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {},
  logo: {
    marginTop: 40,
    marginBottom: 100,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 5,
  },
});

export default LoginScreen;
