import React from 'react';
import {View, StyleSheet, useWindowDimensions} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';

import AppText from '../../components/common/AppText';
import Screen from '../../components/common/Screen';
import BannerLogo from '../../components/common/BannerLogo';
import HomeTabButton from '../../components/home/HomeTabButton';
import defaultStyle from '../../config/styles';
import ScanFragment from '../../components/home/ScanFragment';
import CategoryListFragment from '../../components/home/CategoryListFragment';
import ShakeToLogout from '../../components/home/ShakeToLogout';

const renderScene = SceneMap({
  scanner: ScanFragment,
  categoryList: CategoryListFragment,
});

const renderTabBar = props => {
  return (
    <>
      <ShakeToLogout />
      <View style={styles.tabContainer}>
        {props.navigationState.routes.map((route, position) => {
          return (
            <HomeTabButton
              onPress={() => props.jumpTo(route.key)}
              style={position === 0 ? styles.tab1 : styles.tab2}
              key={route.key}
              isSelected={props.navigationState.index === position}
              icon={route.image}
              title={route.title}
            />
          );
        })}
      </View>
    </>
  );
};

function HomeScreen({route}) {
  const wareHouse = route.params;
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {
      key: 'scanner',
      title: 'Scan QR Code',
      image: require('../../assets/ic_qr.png'),
    },
    {
      key: 'categoryList',
      title: 'Select item by category',
      image: require('../../assets/ic_search.png'),
    },
  ]);

  return (
    <Screen style={styles.container}>
      <View style={styles.header}>
        <BannerLogo />
        <AppText numberOfLines={1} style={styles.warehouseName}>
          {wareHouse.name}
        </AppText>
      </View>

      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        renderTabBar={renderTabBar}
        initialLayout={{width: layout.width}}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {},
  header: {
    backgroundColor: defaultStyle.colors.primary,
    alignItems: 'center',
  },
  warehouseName: {
    color: defaultStyle.colors.white,
    fontSize: 25,
    fontWeight: '700',
    textTransform: 'uppercase',
  },
  tabContainer: {
    flexDirection: 'row',
    paddingVertical: 8,
    justifyContent: 'space-between',
    backgroundColor: defaultStyle.colors.primary,
  },
  tab1: {
    flex: 1,
    marginStart: 6,
    marginEnd: 3,
  },
  tab2: {
    flex: 1,
    marginStart: 3,
    marginEnd: 6,
  },
});

export default HomeScreen;
