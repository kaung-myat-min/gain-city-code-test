import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Yup from 'yup';

import Screen from '../components/common/Screen';
import defaultStyle from '../config/styles';
import BannerLogo from '../components/common/BannerLogo';
import {StackActions} from '@react-navigation/native';
import {AppForm, AppFormField, AppSubmitButton} from '../components/form';

const validationSchema = Yup.object().shape({
  currentPassword: Yup.string().required().min(4).label('Current Password'),
  password: Yup.string().required().min(4).label('Password'),
});

function LoginScreen({navigation}) {
  const handleSubmit = ({staffId, password}) => {
    //todo: call server
    console.log(staffId, password);
    navigation.dispatch(StackActions.popToTop());
  };

  return (
    <Screen style={{backgroundColor: defaultStyle.colors.primary}}>
      <LinearGradient
        colors={[
          defaultStyle.colors.gradientStart,
          defaultStyle.colors.gridientMiddle,
          defaultStyle.colors.gradientEnd,
        ]}
        style={styles.linearGradient}>
        <BannerLogo style={styles.logo} />
        <AppForm
          initialValues={{currentPassword: '', password: ''}}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}>
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            name="currentPassword"
            placeholder="Current Password"
          />
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            name="password"
            placeholder="New Password"
            secureTextEntry
            textContentType="password"
          />
          <AppSubmitButton title="Update" />
        </AppForm>
      </LinearGradient>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {},
  logo: {
    marginTop: 40,
    marginBottom: 100,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 5,
  },
});

export default LoginScreen;
