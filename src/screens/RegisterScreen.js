import React from 'react';
import {StyleSheet, Image, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AppButton from '../components/common/AppButton';

import AppTextButton from '../components/common/AppTextButton';
import AppTextInput from '../components/common/AppTextInput';
import AppText from '../components/common/AppText';
import Screen from '../components/common/Screen';
import defaultStyle from '../config/styles';

function RegisterScreen(props) {
  return (
    <Screen style={{backgroundColor: defaultStyle.colors.primary}}>
      <LinearGradient
        colors={[
          defaultStyle.colors.gradientStart,
          defaultStyle.colors.gridientMiddle,
          defaultStyle.colors.gradientEnd,
        ]}
        style={styles.linearGradient}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../assets/ic_logo.png')}
          />
          <AppText style={styles.logoTitle}>Gain City</AppText>
        </View>
        <AppTextInput placeholder="email" />
        <AppTextInput placeholder="email" />
        <AppButton title="login" color={defaultStyle.colors.white} />
        <AppTextButton
          title="Change Password?"
          color={defaultStyle.colors.transparent}
        />
      </LinearGradient>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {},
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 5,
  },
  logo: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 50,
    marginBottom: 20,
  },
  logoTitle: {
    textTransform: 'uppercase',
    fontSize: 30,
    fontWeight: '700',
  },
});

export default RegisterScreen;
