/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';

import {navigationRef} from './navigation/rootNavigation';
import navigationTheme from './navigation/navigationTheme';
import authStorage from './auth/storage';
import AppNavigator from './navigation/AppNavigator';
import AuthNavigator from './navigation/AuthNavigator';
import AuthContext from './auth/context';

const App = () => {
  const [user, setUser] = useState();
  const [isReady, setIsReady] = useState(false);

  const restoreUser = async () => {
    const user = await authStorage.fetchUser();
    if (user) setUser(user);
  };
  useEffect(() => {
    const getAsyncData = async () => {
      restoreUser();
      setIsReady(true);
    };
    getAsyncData();
  }, []);

  //todo: implement splash screen
  if (!isReady) return null;

  return (
    <AuthContext.Provider value={{user, setUser}}>
      <NavigationContainer ref={navigationRef} theme={navigationTheme}>
        {user ? <AppNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
};
export default App;
